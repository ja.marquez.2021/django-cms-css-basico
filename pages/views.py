import random

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Page

html_template = """<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8" />
    <link href="main.css" rel="stylesheet">
    <title>Django CMS</title>
  </head>
  <body>
    {body}
  </body>
</html>
"""

html_item_template = "<li><a href='{name}'>{name}</a></li>"

html_content_template = "<p> {content} </p>"


def index(request):
    pages = Page.objects.all()
    if len(pages) == 0:
        body = "No pages yet."
    else:
        body = "<ul>"
        for p in pages:
            body += html_item_template.format(name=p.name)
        body += "</ul>"
    return HttpResponse(html_template.format(body=body))


@csrf_exempt
def page(request, name):
    if request.method == 'PUT':
        try:
            p = Page.objects.get(name=name)
        except Page.DoesNotExist:
            p = Page(name=name)
        p.content = request.body.decode("utf-8")
        p.save()

    if request.method == 'GET' or request.method == 'PUT':
        try:
            p = Page.objects.get(name=name)
            content = p.content
            contentToPage = html_content_template.format(content=content)
            response = (HttpResponse(html_template.format(body=contentToPage)))
        except Page.DoesNotExist:
            content = "Page " + name + " not found"
            response = (HttpResponse(html_template.format(body=content)))
        return response


def style(request):
    colores_html = ['yellow', 'blue', 'white', 'gold', 'gray', 'brown',
                    'purple', 'orange', 'black', 'silver', 'red', 'pink',
                    'green']
    color_color = random.choice(colores_html)
    color_background = random.choice(colores_html)

    content = """
    body {
margin: 10px 20% 50px 70px;
font-family: sans-serif;
color: """ + color_color + """;
background: """ + color_background + """;
}"""
    # esto es para que el tipo de texto se css y no html
    content_type = 'text/css'
    return HttpResponse(content, content_type=content_type)

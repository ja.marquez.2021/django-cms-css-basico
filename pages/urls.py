"""items URL Configuration
"""

from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('main.css', views.style, name='style'),
    path('<name>', views.page, name='page')
]
